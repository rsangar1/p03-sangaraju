//
//  GameScreen.h
//  p03-sangaraju
//
//  Created by Ramesh Sangaraju on 2/12/16.
//

#import <UIKit/UIKit.h>

@interface GameScreen : UIView
{
	float dx, dy;  // Ball motion
    int score;
}

@property (nonatomic, strong) UIView *paddle;
@property (nonatomic, strong) UIView *ball;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) UIView *tile;
@property (nonatomic, strong) NSMutableArray *tilesArray;

@property (strong, nonatomic) IBOutlet UITextField *scoreField;
@property (strong, nonatomic) IBOutlet UILabel *labelGameOver;
@property (strong, nonatomic) IBOutlet UIButton *tryAgainButton;


-(void)createPlayField;
-(void)resetPlayField;

@end
