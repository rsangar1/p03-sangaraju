//
//  GameScreen.m
//  p03-sangaraju
//
//  Created by Ramesh Sangaraju on 2/12/16.

#import <QuartzCore/QuartzCore.h>
#import "GameScreen.h"

@implementation GameScreen
@synthesize paddle, ball, tile;
@synthesize timer;
@synthesize labelGameOver, scoreField, tryAgainButton;

int score;

//method to create play field
-(void)createPlayField
{
    //setting background image
    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"sky.jpg"]];
    
    //creating paddle
    paddle = [[UIView alloc] initWithFrame:CGRectMake(175, 400, 150, 10)];
	[self addSubview:paddle];
	[paddle setBackgroundColor:[UIColor greenColor]];
    paddle.layer.cornerRadius = 5;
    paddle.layer.masksToBounds = YES;
    paddle.clipsToBounds = YES;
	
    //creating ball
	ball = [[UIView alloc] initWithFrame:CGRectMake(235, 380, 15, 15)];
	[self addSubview:ball];
	[ball setBackgroundColor:[UIColor redColor]];
    ball.layer.cornerRadius = 8;
    ball.layer.masksToBounds = YES;
    
	dx = 10;
	dy = 10;
    
    //creating bricks
    //allocating memory to the array of tiles
    self.tilesArray = [[NSMutableArray alloc]init];
    
    [self createTilesArray];
    
    //creating a label
    //Ref: http://stackoverflow.com/questions/17813121/programmatically-creating-uilabel
    labelGameOver = [[UILabel alloc]initWithFrame:CGRectMake(160, 200, 120, 30)];
    labelGameOver.text = @"You Lost :(";
    labelGameOver.font = [UIFont systemFontOfSize:20];
    labelGameOver.textColor = [UIColor orangeColor];
    labelGameOver.textAlignment = NSTextAlignmentCenter;
    labelGameOver.clipsToBounds = YES;
    labelGameOver.backgroundColor = [UIColor blackColor];
    [self addSubview:labelGameOver];
    
    //creating a button
    //Ref: http://stackoverflow.com/questions/1378765/how-do-i-create-a-basic-uibutton-programmatically
    tryAgainButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [tryAgainButton addTarget:self action:@selector(resetPlayField) forControlEvents:UIControlEventTouchUpInside];
    [tryAgainButton setTitle:@"Try Again!" forState:UIControlStateNormal];
    tryAgainButton.frame = CGRectMake(170.0, 250.0, 100.0, 30.0);
    [self addSubview:tryAgainButton];
    tryAgainButton.backgroundColor = [UIColor blackColor];
    
    //creating score field
    //Ref: http://stackoverflow.com/questions/15690308/creating-text-fields-programmatically
    scoreField = [[UITextField alloc] initWithFrame:CGRectMake(225, 15, 50, 25)];
    scoreField.textColor = [UIColor colorWithRed:0/256.0 green:84/256.0 blue:129/256.0 alpha:1.0];
    scoreField.font = [UIFont fontWithName:@"Helvetica-Bold" size:16];
    scoreField.backgroundColor=[UIColor whiteColor];
    scoreField.text=@"0";
    scoreField.textAlignment = NSTextAlignmentCenter;
    [self addSubview:scoreField];
    
    
    labelGameOver.hidden = YES;
    tryAgainButton.hidden = YES;
}

-(void)createTilesArray{
    int startY = 50;
    int brick_height = 12;
    int brick_width = 47;
    for(int i=0;i<5;i++){
        for(int j=0;j<10;j++){
            UIView *thisTile = [[UIView alloc] initWithFrame:CGRectMake(j*50, startY+(i*brick_height), brick_width, brick_height-2)];
            [self addSubview:thisTile];
            [thisTile setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Brick_Red.gif"]]];
            [self.tilesArray addObject:thisTile];
        }
    }
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
	for (UITouch *t in touches)
	{
		CGPoint p = [t locationInView:self];
        p.y = 400;
		[paddle setCenter:p];
	}
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
	[self touchesBegan:touches withEvent:event];
}

-(IBAction)startAnimation:(id)sender
{
	timer = [NSTimer scheduledTimerWithTimeInterval:.1	target:self selector:@selector(timerEvent:) userInfo:nil repeats:YES];
    
}

-(IBAction)stopAnimation:(id)sender
{
	[timer invalidate];
}

-(void)timerEvent:(id)sender
{
	CGRect bounds = [self bounds];
	
	// NSLog(@"Timer event.");
	CGPoint ballCGP = [ball center];
    CGPoint paddleCGP = [paddle center];
	if ((ballCGP.x + dx) < 0)
		dx = -dx;
	
	if ((ballCGP.y + dy) < 0)
		dy = -dy;
	
	if ((ballCGP.x + dx) > bounds.size.width)
		dx = -dx;
	
	if ((ballCGP.y + dy) > bounds.size.height)
		dy = -dy;
	
	ballCGP.x += dx;
	ballCGP.y += dy;
	[ball setCenter:ballCGP];
    
    //checking if paddle is going out of bounds of game screen
    if(paddleCGP.x < (paddle.bounds.size.width/2)){
        //NSLog(@"satisfiled");
        paddleCGP.x = paddle.bounds.size.width/2;
    }
    if((paddleCGP.x + (paddle.bounds.size.width/2) > bounds.size.width)){
        paddleCGP.x = bounds.size.width - (paddle.bounds.size.width/2);
    }
    [paddle setCenter:paddleCGP];
	
	// Now check to see if we intersect with paddle.  If the movement
	// has placed the ball inside the paddle, we reverse that motion
	// in the Y direction.
	if (CGRectIntersectsRect([ball frame], [paddle frame]))
	{
		dy = -dy;
		ballCGP.y += 2*dy;
		[ball setCenter:ballCGP];
	}
	
    /* //for debugging
    int count = (int)_tilesArray.count;
    scoreField.text = [NSString stringWithFormat:@"%d", count];
    NSLog(@"count is:%d", _tilesArray.count); */
    
    //check if ball intersect with tiles
    [self checkTilesCollision];
    
    //display score
    scoreField.text = [NSString stringWithFormat:@"%d", score];
    
    //check if ball is moving down the paddle
    if(ballCGP.y > 410){
        score = 0;
        ball.hidden = YES;
        paddle.hidden = YES;
        labelGameOver.hidden = NO;
        tryAgainButton.hidden = NO;
        [self stopAnimation:(self)];
    }

}

//this method helps to find if the ball collides with tiles
- (void) checkTilesCollision{
    for(int i=(int)_tilesArray.count-1;i>=0;i--){
        UIView *thisTile = self.tilesArray[i];
        if(CGRectIntersectsRect([ball frame], [thisTile frame])){
            CGPoint p = [ball center];
            dy = -dy;
            p.y += 2*dy;
            [ball setCenter:p];
            thisTile.hidden=YES;
            [thisTile removeFromSuperview];
            [self.tilesArray removeObject:thisTile];
            score = score + 100;
        }
    }
    //if all the tiles are cleard, message appears
    if((int)self.tilesArray.count <=0){
        labelGameOver.text = @"You Won :)";
        labelGameOver.hidden = NO;
        tryAgainButton.hidden = NO;
    }
}

- (void)resetPlayField{
    [paddle removeFromSuperview];
    [ball removeFromSuperview];
    [scoreField removeFromSuperview];
    [labelGameOver removeFromSuperview];
    [tryAgainButton removeFromSuperview];
    
    for(int i=(int)_tilesArray.count-1;i>=0;i--){
        UIView *thisTile = self.tilesArray[i];
        [thisTile removeFromSuperview];
    }
    [self createPlayField];
}

@end
