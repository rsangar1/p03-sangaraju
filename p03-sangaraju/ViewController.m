//
//  ViewController.m
//  breakout
//
//  Created by Ramesh Sangaraju on 2/12/16.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize gameScreen;


- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"squares.jpg"]];
	[gameScreen createPlayField];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
