//
//  ViewController.h
//  p03-sangaraju
//
//  Created by Ramesh Sangaraju on 2/12/16.

#import <UIKit/UIKit.h>
#import "GameScreen.h"

@interface ViewController : UIViewController
@property (nonatomic, strong) IBOutlet GameScreen *gameScreen;

@end

